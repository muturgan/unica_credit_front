import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IUser } from '../../custom_types';
import { RestService } from '../../services/rest.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

   @ViewChild('firstNameInput', { static: false }) firstNameInput: ElementRef<HTMLInputElement>;
   @ViewChild('lastNameInput', { static: false }) lastNameInput: ElementRef<HTMLInputElement>;

   public users: IUser[] = [];
   public targetUser: IUser | null = null;
   public isOneFetching = false;
   public isManyFetching = false;
   public init = false;
   public fetchingManyErrorMessage = '';
   public fetchingOneErrorMessage = '';

   constructor(private _http: RestService) {}

   ngOnInit() {
      this._http.fetchingOne$.subscribe(
         () => {
            this.init = true;
            this.isOneFetching = !this.isOneFetching;
         },
      );
      this._http.fetchingMany$.subscribe(
         () => this.isManyFetching = !this.isManyFetching,
      );

      this._http.fetchUsers()
         .subscribe(
            res => {
               this.users = res;
               this.fetchingManyErrorMessage = '';
            },
            err => {
               this.fetchingManyErrorMessage = err.error.message;
            }
         );
   }


   public onSearch() {
      const first_name = this.firstNameInput.nativeElement.value;
      const last_name = this.lastNameInput.nativeElement.value;

      this._http.findUser(first_name, last_name)
         .subscribe(
            res => {
               this.targetUser = res;
               this.fetchingOneErrorMessage = '';
            },
            err => {
               this.targetUser = null;
               this.fetchingOneErrorMessage = err.error.status === 404
                  ? ''
                  : err.error.message;
            }
         );
   }


   public onChange(perPageInput: HTMLInputElement, pageInput: HTMLInputElement) {
      const perPage = perPageInput.value;
      const page = pageInput.value;
      console.log(perPage);
      console.log(page);

      this._http.fetchUsers(perPage, page)
         .subscribe(
            res => {
               this.users = res;
               this.fetchingManyErrorMessage = '';
            },
            err => {
               this.fetchingManyErrorMessage = err.error.message;
            }
         );
   }

}
