import { environment } from '../../environments/environment';

export const baseUrl = environment.production
   ? '/api/'
   : 'http://localhost:3333/api/';
