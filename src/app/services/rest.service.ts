import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { IUser } from '../custom_types';
import { baseUrl } from '../constants';

@Injectable()
export class RestService {

   public fetchingMany$ = new Subject<void>();
   public fetchingOne$ = new Subject<void>();

   constructor(private _http: HttpClient) {}


   public fetchUsers(per_page?: string, page?: string): Observable<IUser[]> {

      let httpParams = new HttpParams();
      if (typeof per_page === 'string') {
         httpParams = httpParams.append('per_page', per_page);
      }
      if (typeof page === 'string') {
         httpParams = httpParams.append('page', page);
      }

      this.fetchingMany$.next();

      const observable = this._http.get<IUser[]>(baseUrl + 'users', {params: httpParams});

      observable
         .subscribe(
            () => this.fetchingMany$.next(),
            () => this.fetchingMany$.next(),
         );

      return observable;
   }


   public findUser(first_name?: string, last_name?: string): Observable<IUser> {

      let httpParams = new HttpParams();
      if (typeof first_name === 'string') {
         httpParams = httpParams.append('first_name', first_name);
      }
      if (typeof last_name === 'string') {
         httpParams = httpParams.append('last_name', last_name);
      }

      this.fetchingOne$.next();

      const observable = this._http.get<IUser>(baseUrl + 'users/find', {params: httpParams});

      observable
         .subscribe(
            () => this.fetchingOne$.next(),
            () => this.fetchingOne$.next(),
         );

      return observable;
   }

}
