import { UsersComponent } from './views/users/users.component';
import { TaskComponent } from './views/task/task.component';
import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { NotFoundComponent } from './views/errors/not-found/not-found.component';


const routes: Route[] = [
   { path: '', pathMatch: 'full', redirectTo: 'users' },
   { path: 'users', component: UsersComponent },
   { path: 'task', component: TaskComponent },
   { path: '**', component: NotFoundComponent },

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
